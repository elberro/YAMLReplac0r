package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"reflect"
	"strings"

	"gopkg.in/yaml.v2"
)

//A CLI flag for silent operation. If set no output will be generated to stdout. Except errors.
var silentPtr = flag.Bool("s", false, "if set, no output will be generated.")

//The source yaml file containing the placeholders
var sourceYAMLPtr = flag.String("src", "", "The source yaml file containing the placeholders")

//The destination yaml file containing the replaced placeholders
var outputYAMLPtr = flag.String("out", "output.yaml", "The destination yaml file containing the replaced placeholders")

//A map containing CLI arguments e.g. [PLACEHOLDER_A: ABC, PLACEHOLDER_B:BCD]
var envVars = map[string]string{}

func main() {
	flag.Parse()

	args := flag.Args()

	//transform CLI args to map. Character '=' separates key and value
	if len(args) > 0 {
		for _, arg := range args {
			pair := strings.SplitN(arg, "=", 2)
			envVars[pair[0]] = pair[1]
		}
	}

	//Loads the source yaml file
	f, err := os.Open(*sourceYAMLPtr)
	if err != nil {
		fmt.Println(err)
	}
	defer f.Close()

	m := make(map[interface{}]interface{})

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&m)
	if err != nil {
		fmt.Println(err)
	}

	for key, value := range m {
		findPlaceholders(&key, &value, m)
	}

	writeYaml(m)

}

//Parameter k: key of a map or slice.
//Parameter v: value of a map or slice.
//Parameter parent: the map or slice of the recursive call before.
//Recursivly examines the map and searches for placeholders.
//Placeholders are defined as all strings starting with the character $
//The function distingueshes between a map, slice and string
func findPlaceholders(k interface{}, v interface{}, parent interface{}) {
	//fmt.Println("original ", turn, ":", k, v, " value: ", *v.(*interface{}))
	//fmt.Println(parent)
	if reflect.Indirect(reflect.ValueOf(v)).Elem().Kind() == reflect.Map {
		yamlMap := *v.(*interface{})
		for key, value := range yamlMap.(map[interface{}]interface{}) {
			findPlaceholders(&key, &value, yamlMap)
		}
	} else if reflect.Indirect(reflect.ValueOf(v)).Elem().Kind() == reflect.Slice {
		yamlSlice := *v.(*interface{})
		for key, value := range yamlSlice.([]interface{}) {
			findPlaceholders(&key, &value, yamlSlice)
		}
	} else {
		if reflect.Indirect(reflect.ValueOf(v)).Elem().Kind() == reflect.String {
			yamlString := *v.(*interface{})
			if yamlString.(string)[0:1] == "$" {
				*v.(*interface{}) = replacePlaceholder(yamlString.(string)[1:])
				parent.(map[interface{}]interface{})[*k.(*interface{})] = *v.(*interface{})
			}
		}
	}
}

//Checks for the proper placeholder in the list of command line arguments
//If no matching key was found the system environment variables will be checked.
//In case of both options failed the string "not_found" will replace the placeholder.
func replacePlaceholder(placeholder string) string {
	if len(envVars) > 0 {
		for key, value := range envVars {
			if placeholder == key {
				return value
			}
		}
	}

	for _, e := range os.Environ() {
		pair := strings.SplitN(e, "=", 2)
		if pair[0] == placeholder {
			return pair[1]
		}
	}

	return "not_found"
}

//Writes the yaml with replaced placeholders to the given path.
func writeYaml(m interface{}) {
	d, err := yaml.Marshal(&m)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	if !*silentPtr {
		fmt.Printf(string(d))
	}

	err = ioutil.WriteFile(*outputYAMLPtr, d, 0644)
	if err != nil {
		log.Fatal(err)
	}

}
