# YAMLReplac0r

This tool replaces placeholders in yaml either by command line arguments or environment variables with the same name. Both mechanisms can be used at the same time, whereas a CLI argument has higher priority.


I created this tool because yaml by default doesn't know placeholders, but I required this functionality for my CI/CD. 

Usage:

--Flags:

    $ yreplac0r -h //get an overview of the application's CLI flags
    $ yreplac0r -s //silent mode. If set no output will be generated to stdout. Except errors.

--Examples:
    
    --Use environment variables to replace placeholders:
        
        $ cat test.yaml
        
            apiVersion: $VERSION
            kind: Service
            metadata:
              annotations:
                  service.beta.kubernetes.io/azure-load-balancer-resource-group: $RESGROUP_PLACEHOLDER
              name: $NAME
            
        $ export VERSION=1.0
        $ export RESGROUP_PLACEHOLDER=Batcave
        $ export NAME=PROXY
        
        $ yreplac0r -src test.yaml -out test_replaced.yaml
        
        $ cat test_replaced.yaml
        
            apiVersion: 1.0
            kind: Service
            metadata:
              annotations:
                  service.beta.kubernetes.io/azure-load-balancer-resource-group: Batcave
              name: PROXY
        

    --Use CLI arguments to replace placeholders:
        
        $ cat test.yaml
        
            apiVersion: $VERSION
            kind: Service
            metadata:
              annotations:
                  service.beta.kubernetes.io/azure-load-balancer-resource-group: $RESGROUP_PLACEHOLDER
              name: $NAME
        
        $ yreplac0r -src test.yaml -out test_replaced.yaml VERSION=1.0 RESGROUP_PLACEHOLDER=Batcave NAME=PROXY
        
        $ cat test_replaced.yaml
        
            apiVersion: 1.0
            kind: Service
            metadata:
              annotations:
                  service.beta.kubernetes.io/azure-load-balancer-resource-group: Batcave
              name: PROXY


A mix of both mechanisms is possible, but keep in mind: CLI arguments win! :) enjoy.